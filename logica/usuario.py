usuarios =[]

def adicionar_usuario(cpf, nome, email, senha):
    usuario = [cpf,nome,email,senha]
    usuarios.append(usuario)

def listar_usuarios():
    return usuarios

def buscar_usuarios(cpf):
    for u in usuarios:
        if (u[0]==cpf):
            return u
    return None

def remover_usuario(cpf):
    for u in usuarios:
        if (u[0]== cpf):
            usuarios.remove(u)
            return True
    return False

def iniciar_usuarios():
    adicionar_usuario(11111111111,"Fellippe","Fellippe.Alfer@gmail.com","1234")
    adicionar_usuario(22222222222,"Matheus","Matheus.silvano@hotmail.com","4321")


