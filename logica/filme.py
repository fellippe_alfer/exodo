filmes = []

codigo_geral = 0
def _gerar_codigo():
    global codigo_geral
    codigo_geral += 1
    return codigo_geral


def adicionar_filmes(titulo, genero, ano):
    filme = [titulo, genero, ano]
    filmes.append(filme)


def listar_filmes():
    return filmes


def buscar_filme(cod_filme):
    for f in filmes:
        if (f[0] == cod_filme):
            return f
        return None


def buscar_filmes_por_genero(genero):
    for f in filmes:
        if (f[1] == genero):
            return f
        return None


def remover_filme(filme):
    for f in filmes:
        if (f[0] == filme):
            filmes.remove(f)
            return True
        return False


def iniciar_filmes():
    adicionar_filmes("Toy Story", "Infantil", 1995)
    adicionar_filmes("Toy Story 2", "Infantil", 2002)